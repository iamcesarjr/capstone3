import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Layout from '../components/Layout';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function Login() {
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);
    const navigate = useNavigate()

    function authenticate(e) {

        e.preventDefault();
        fetch('https://cpstn2-ecommerceapi-delacruz.onrender.com/users/login', {

            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {

                console.log("from login")

                if (typeof data.access !== "undefined") {

                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);
                    setUser({
                        access: localStorage.getItem('token')
                    })
                    Swal.fire({
                        title: "Authentication Success",
                        icon: "success",
                        text: "Logged-in Successfully!"
                    })
                    .then(() => {
                        navigate('/')
                    })
                    
                } else {
                    Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Please check your email and password"
                    })
                }
            })
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch('https://cpstn2-ecommerceapi-delacruz.onrender.com/users/details', {
            headers: {
                Authorization: `Bearer ${token} `
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    useEffect(() => {

        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email, password]);
    return (
        <Layout>
            <div className='login'>
            <Form onSubmit={(e) => authenticate(e)}>
                <h1 className="my-5 text-center" style={{fontWeight: 'bold'}}>Login</h1>
                <Form.Group controlId="userEmail">
                    <Form.Label style={{fontWeight: 'bold'}}>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label style={{fontWeight: 'bold'}}>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {isActive ?
                    <Button className="mt-3" variant="primary" type="submit" id="submitBtn" to="/products">
                        Submit
                    </Button>
                    :
                    <Button className="mt-3" variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </Form>
            </div>
        </Layout>
    )
}