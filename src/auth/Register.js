import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Layout from '../components/Layout';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register () {
    const { user } = useContext(UserContext);

    const [firstName, setFirstName] = useState("");

    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    const navigate = useNavigate();


    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(confirmPassword);

    function registerUser(e) {
        e.preventDefault();
        fetch('https://cpstn2-ecommerceapi-delacruz.onrender.com/users/register', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({

                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password

            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data) {
                    Swal.fire({
                        title: "Registered Successfully!",
                        icon: "success",
                        text: "Please login to Access Website"
                    })
                    .then(() =>{
                        navigate('/login')
                    })
                    setFirstName('');
                    setLastName('');
                    setEmail('');
                    setMobileNo('');
                    setPassword('');
                    setConfirmPassword('')
                } else {
                    Swal.fire({
                        title: "Error",
                        icon: "error",
                        text: "Please try again"
                    })
                }
            })
    }
    useEffect(() => {

        if ((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)) {

            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, email, mobileNo, password, confirmPassword])
    return (
        <Layout>
            <div className="register">
                <Form onSubmit={(e) => registerUser(e)}>
                    <h1 className="mt-4 py-3 text-center" style={{fontWeight: 'bold'}}>Create Your Free Account</h1>
                    <Form.Group>
                        <Form.Label style={{fontWeight: 'bold'}}>First Name:</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter First Name"
                            required
                            value={firstName}
                            onChange={e => { setFirstName(e.target.value) }}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label style={{fontWeight: 'bold'}}>Last Name:</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter Last Name"
                            required
                            value={lastName}
                            onChange={e => { setLastName(e.target.value) }}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label style={{fontWeight: 'bold'}}>Email:</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter Email"
                            required
                            value={email}
                            onChange={e => { setEmail(e.target.value) }}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label style={{fontWeight: 'bold'}}>Mobile No:</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter 11-digit No."
                            required
                            value={mobileNo}
                            onChange={e => { setMobileNo(e.target.value) }}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label style={{fontWeight: 'bold'}}>Password: </Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Enter Password"
                            required
                            value={password}
                            onChange={e => { setPassword(e.target.value) }}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label style={{fontWeight: 'bold'}}>Confirm Password: </Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Confirm Password"
                            required
                            value={confirmPassword}
                            onChange={e => { setConfirmPassword(e.target.value) }}
                        />
                    </Form.Group>
                    {
                        isActive ?
                            <Button className="mt-3" variant="primary" type="submit" id="submitBtn" to="/login">Submit</Button>

                            :
                            <Button className="mt-3" variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                    }
                </Form>
            </div>
        </Layout>
    )
}