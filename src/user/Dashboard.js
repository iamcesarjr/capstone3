import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Layout from '../components/Layout';
import UserMenu from '../components/UserMenu';

export default function UserDashboard () {
    
    
    return(
        <Layout>
            <Container fluid>
                <Row className='mt-5'>
                    <Col md={3} sm={12}>
                        <UserMenu />
                    </Col>
                    <Col md={9} sm={12}>
                      <h1>Welcome to Your Dashboard</h1>
                    </Col>
                </Row>
            </Container>
        </Layout>
    )
}