import React, { useContext, useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import { GrSubtractCircle, GrAddCircle } from 'react-icons/gr'

export default function EditCart({ productId, quantity, fetchData}) {

  const { user } = useContext(UserContext);

  const increaseCartQuantity = () =>{
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/cart/${productId}/increase`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      fetchData()
    })
    .catch(err => console.log(err))
  }

  const decreaseCartQuantity = () =>{
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/cart/${productId}/decrease`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      fetchData()
    })
    .catch(err => console.log(err))
  }

  return (
    <>
      <Button className="mx-3" variant="success" onClick={() => increaseCartQuantity(quantity)}> <GrAddCircle /> </Button>
      <Button variant="warning" onClick={() => decreaseCartQuantity(quantity)}> <GrSubtractCircle  /> </Button>
    </>
  );
  }