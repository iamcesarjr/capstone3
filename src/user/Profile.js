import React from 'react'
import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Layout from '../components/Layout'
import UserMenu from '../components/UserMenu';
import UpdateProfile from './UpdateProfile';
import ResetPassword from './ResetPassword';

export default function Profile () {
    
    const {user} = useContext(UserContext);

    const [ details, setDetails ] = useState({})

    const updateProfileDetails = (newDetails) => {
      setDetails(newDetails);
    };

    useEffect(()=>{

        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/details`,{
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token')}`
            }
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data)

            if(typeof data.id !== undefined){
                setDetails(data);
            }
        })
    },[])
    return(
        <Layout>
            <Container fluid>
                <Row className='mt-5'>
                    <Col md={3} sm={12}>
                        <UserMenu />
                    </Col>
                    <Col md={9} sm={12}>
                        <h1 style={{color:'#5E17EB'}}><strong>My Account Details</strong></h1>
                        <h2><strong>{`${details.firstName} ${details.lastName}`}</strong></h2>
                        <h4>Contacts</h4>
                        <ul>
                            <li>Email: {details.email}</li>
                            <li>Mobile No: {details.mobileNo}</li>
                        </ul>
                        <UpdateProfile user={details} updateProfileDetails={updateProfileDetails}/>
                        <ResetPassword user={details} />
                    </Col>
                </Row>
            </Container>
        </Layout>
    )
}