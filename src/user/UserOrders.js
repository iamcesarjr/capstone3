import React, { useContext, useState, useEffect } from 'react';
import Layout from '../components/Layout';
import { Col, Container, Row, Table } from 'react-bootstrap';
import UserMenu from '../components/UserMenu';
import UserContext from '../UserContext';

export default function UserOrders() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  useEffect(() => {

    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/${user._id}/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrders(data);
      });
  }, [user._id]);

  return (
    <Layout>
      <Container fluid>
        <Row className='mt-5'>
          <Col className='col-md-3'>
            <UserMenu />
          </Col>
          <Col className='col-md-9'>
          <h1 style={{color:'#5E17EB'}}><strong>My Orders</strong></h1>
            <Table striped bordered>
              <thead>
                <tr>
                  <th>ProductId</th>
                  <th>Product Name</th>
                  <th>Date Placed</th>
                  <th>Quantity</th>
                  <th>Total</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {orders.map((order) => (
                  <tr key={order._id}>
                    <td>{order.productId}</td>
                    <td>{order.name}</td>
                    <td>{order.orderPlaced}</td>
                    <td>{order.quantity}</td>
                    <td>${order.totalPrice}</td>
                    <td>{order.status}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}
