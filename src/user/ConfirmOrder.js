import { useContext } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";


export default function ConfirmOrder ({ item, user }) {

    const handleConfirmCartOrder = () => {
        Swal.fire({
          text: 'Do you want to place order for this item?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Yes',
          cancelButtonText: 'Cancel',
        }).then((result) => {
          if (result.isConfirmed) {
            placeOrderOnServer(item);
          }
        });
      };

      const placeOrderOnServer = (item) => {
        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/confirmOrder`, {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify({})
        })
          .then((res) => {
            if (res.ok) {
              return res.json();
            } else {
              throw new Error('Network response was not ok');
            }
          })
          .then((data) => {
            console.log(data);
            if(data === true){
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Order placed successfully',
                  });
            }else{
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Failed to place the order. Please try again.',
                  });
            }
          })
          .catch(err => console.log(err))
      };

    return(

        <Button onClick={handleConfirmCartOrder}>Place Order</Button>
    )
}