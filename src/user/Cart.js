import React, { useState, useEffect, useContext } from 'react';
import Layout from '../components/Layout';
import { Container, Row, Col, Table } from 'react-bootstrap';
import UserMenu from '../components/UserMenu';
import UserContext from '../UserContext';
import EditCart from './EditCart';
import DeleteItem from './DeleteItem';
import ConfirmOrder from './ConfirmOrder';

export default function UserCart() {
  const [cart, setCart] = useState([]);
  const [ user, setUser ] = useState("")

  useEffect(() => {

    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/details`,{
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
          setUser(data)
          setCart(data.cart)
        })
      })
  return (
    <Layout>
      <Container fluid>
        <Row className='mt-5'>
          <Col md={3} sm={12}>
            <UserMenu />
          </Col>
          <Col md={9} sm={12}>
          <h1 style={{color:'#5E17EB'}}><strong>My Cart</strong></h1>
            <Table striped bordered>
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Unit Price</th>
                  <th>Quantity</th>
                  <th>Subtotal</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {cart.map((item) => (
                  <tr key={item.productId}>
                    <td>{item.name}</td>
                    <td>${item.price}</td>
                    <td>{item.quantity} <EditCart productId={item.productId} quantity={item.quantity} /></td>
                    <td>${item.subtotal}</td>
                    <td><DeleteItem item={item}/> </td>
                    <ConfirmOrder item={item} user={user} />
                  </tr>
                  
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}
