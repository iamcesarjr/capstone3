import { Button } from "react-bootstrap";
import Swal from "sweetalert2";
import {RiDeleteBin6Line} from 'react-icons/ri'

export default function DeleteItem({ item }) {
  const handleDeleteCartItem = () => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'This product will be removed from your cart',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        deleteCartItemRequest();
      }
    });
  };

  const deleteCartItemRequest = () => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/cart/${item.productId}/deleteItem`, {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then((res) => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error('Network response was not ok');
      }
    })
    .then((data) => {
      console.log(data);
      if (data === true) {
        Swal.fire({
          title: 'Success',
          icon: 'success',
          text: 'Product deleted',
        });
      } else {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Please try again',
        });
      }
    })
    .catch((error) => {
      console.error(error);
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'An error occurred while deleting the product',
      });
    });
  };

  return (
    <Button variant="danger" size="sm" onClick={handleDeleteCartItem}> <RiDeleteBin6Line /></Button>
  );
}
