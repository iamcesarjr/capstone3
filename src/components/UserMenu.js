import { NavLink } from "react-router-dom";
import { ListGroup } from "react-bootstrap";

export default function UserMenu () {

    return (
        <ListGroup>
            <ListGroup.Item action href="/myAccount">
                My Account
            </ListGroup.Item>
            <ListGroup.Item action href="/myCart">
                My Cart
            </ListGroup.Item>
            <ListGroup.Item action href="/myOrder">
                My Orders
            </ListGroup.Item>
        </ListGroup>
    )
}