import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from '../images/logo.JPG'
import {BsLinkedin, BsGithub} from 'react-icons/bs'
import {BiLogoGitlab} from 'react-icons/bi'

export default function Footer () {
    return (
        <div className="footer">
            <Container fluid>
                <Row>
                    <Col md={4}>
                        <img src={logo}/>
                        <p className="my-2" style={{ color: '#FFFFFF' }}>
                            All Right Reserved &copy; Game Zone
                        </p>
                    </Col>
                    <Col md={4}>
                        <div className="my-3">
                            <Link to="/contact" style={{ color: '#FFFFFF' }}>Support </Link>
                        </div>
                        <div className="my-3">
                            <Link to="/about" style={{ color: '#FFFFFF' }}>About Us </Link>
                        </div>
                        <div className="my-3">
                            <h5  style={{ color: '#FFFFFF' }}>Privacy Policy </h5>
                        </div>
                        <div className="my-3">
                            <h5  style={{ color: '#FFFFFF' }}>Website Terms of Use </h5>
                        </div>
                        <div className="my-3">
                            <h5 style={{ color: '#FFFFFF' }}>Sitemap</h5>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="my-3">
                            <p style={{ color: '#FFFFFF' }}>This wesbite is for demo purposes only.</p>
                            <p style={{ color: '#FFFFFF' }}>Contact Creator:</p>
                        </div>
                        <div className="my-3">
                            <Link to="https://www.linkedin.com/in/cesardelacruzjr/" style={{ color: '#FFFFFF' }}><BsLinkedin /> LinkedIn</Link>
                        </div>
                        <div className="my-3">
                            <Link to="https://github.com/cesardcjr" style={{ color: '#FFFFFF' }}><BsGithub /> GitHub</Link>
                        </div>
                        <div className="my-3">
                            <Link to="https://gitlab.com/iamcesarjr" style={{ color: '#FFFFFF' }}><BiLogoGitlab /> GitLab</Link>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}