import { NavLink } from "react-router-dom";
import { ListGroup } from "react-bootstrap";

export default function AdminMenu () {

    return (
        <ListGroup defaultActiveKey="#link1">
            <ListGroup.Item action href="/userList" >
                List of Registered Users
            </ListGroup.Item>
            <ListGroup.Item action href="/productCatalog">
                Product Catalog
            </ListGroup.Item>
            <ListGroup.Item action href="/orderManager">
                Order Manager
            </ListGroup.Item>
        </ListGroup>
    )
}