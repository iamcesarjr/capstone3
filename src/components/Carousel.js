import Carousel from 'react-bootstrap/Carousel';
import ps from '../images/ps.jpg'
import fort from '../images/fort.JPG'
import re from '../images/re.JPG'
import nba from '../images/nba.JPG'

export default function ImageCarousel() {
  return (
    <div id="carouselExampleFade" className="carousel slide carousel-fade" data-ride="carousel">
      <div className="carousel-inner">
        <div className="carousel-item active">
          <img src={ps} className="d-block w-100" alt="..." />
        </div>
        <div className="carousel-item">
          <img src={nba} className="d-block w-100" alt="..." />
        </div>
        <div className="carousel-item">
          <img src={re} className="d-block w-100" alt="..." />
        </div>
        <div className="carousel-item">
          <img src={fort} className="d-block w-100" alt="..." />
        </div>
      </div>
      <button className="carousel-control-prev" type="button" data-target="#carouselExampleFade" data-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true" />
        <span className="sr-only">Previous</span>
      </button>
      <button className="carousel-control-next" type="button" data-target="#carouselExampleFade" data-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true" />
        <span className="sr-only">Next</span>
      </button>
    </div>

  );
}
