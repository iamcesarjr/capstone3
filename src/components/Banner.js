import React, { useContext } from 'react';
import UserContext from '../UserContext';
import { Container, Row, Col } from 'react-bootstrap';
import ps5 from '../images/ps5.png'

export default function Banner() {
  const { user } = useContext(UserContext);

  return (

    <Container fluid className='my-3'>
      <Row>
        <Col md={7} sm={12}>
          <div className="label">
            <p className="text-wrapper">Elevate Your Gaming With <span className="playstation-text">Playstation</span></p>
          </div>

        </Col>
        <Col md={5} sm={12}>
          <img src={ps5} style={{ width: '400px', height: '550px' }} alt="banner" />
        </Col>
      </Row>
    </Container>
  )
}
