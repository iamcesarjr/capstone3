import { useState, useContext } from "react";
import { Link, NavLink } from "react-router-dom";
import { Container, Navbar, Nav, Form, Button } from 'react-bootstrap'
import { HiOutlineShoppingCart } from 'react-icons/hi'
import logo from '../images/logo.JPG'

import UserContext from "../UserContext";

export default function Header () {

    const { user } = useContext(UserContext);

    return(
        <>
            <Navbar id="navbar" expand="lg">
                <Container fluid>
                    <Navbar.Brand  as={Link} to="/"><img src={logo} style={{ width: '75px', height: '50px' }} alt="Logo"/></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id='basic-navbar-nav'>
                        <Nav className="ml-auto">
                            <Nav.Link as={Link} to="/" style={{ color: '#FFFFFF' }}>Home |</Nav.Link>
                            <Nav.Link as={Link} to="/products" style={{ color: '#FFFFFF' }}> Products |</Nav.Link>
                            {(user.id !== null) ?
                                user.isAdmin
                                    ?
                                    <>
                                        <Nav.Link as={Link} to="/userList" style={{ color: '#FFFFFF' }}> User's List |</Nav.Link>
                                        <Nav.Link as={Link} to="/orderManager" style={{ color: '#FFFFFF' }}> Order Manager |</Nav.Link>
                                        <Nav.Link as={Link} to="/productCatalog" style={{ color: '#FFFFFF' }}> Product Catalog |</Nav.Link>
                                        <Nav.Link as={Link} to="/logout" style={{ color: '#FFFFFF' }}> Logout</Nav.Link>
                                    </>
                                    :
                                    <>  
                                        <Nav.Link as={Link} to="/myAccount" style={{ color: '#FFFFFF' }}> Profile |</Nav.Link>
                                        <Nav.Link as={Link} to="/myOrder" style={{ color: '#FFFFFF' }}> Orders |</Nav.Link>
                                        <Nav.Link as={Link} to="/myCart" style={{ color: '#FFFFFF', textDecoration: 'none' }}>
                                            <span style={{ marginRight: '8px' }}>
                                                <HiOutlineShoppingCart size={24} />
                                            </span>
                                            My Cart |
                                        </Nav.Link>
                                        <Nav.Link as={Link} to="/logout" style={{ color: '#FFFFFF' }}> Logout</Nav.Link>
                                    </>
                                :
                                <>
                                    <Nav.Link as={Link} to="/login" style={{ color: '#FFFFFF' }}> Sign-in |</Nav.Link>
                                    <Nav.Link as={Link} to="/register" style={{ color: '#FFFFFF' }}> Create Account</Nav.Link>
                                </>
                            }

                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}