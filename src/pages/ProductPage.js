import React, { useEffect, useState } from "react";
import Layout from "../components/Layout";
import Products from "./Products";
import { Button, Col, Container, Form, ListGroup, ListGroupItem, Row } from "react-bootstrap";

export default function ProductPage() {
    const [filteredProducts, setFilteredProducts] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState('All'); 
    const [searchQuery, setSearchQuery] = useState('');
    const [products, setProducts] = useState([]);
    const [minPrice, setMinPrice] = useState(0);
    const [maxPrice, setMaxPrice] = useState(1000); 

    useEffect(() => {
        fetchAllProducts();
    }, []); 

    const fetchAllProducts = () => {
        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/all`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                setProducts(data);
            });
    };

    const handleCategorySelect = (category) => {
        setSelectedCategory(category);

        if (category === 'All') {
            setFilteredProducts(products);
        } else {
            
            const filtered = products.filter((product) => product.category === category);
            setFilteredProducts(filtered);
        }
    };

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };

    const handleSearchSubmit = (e) => {
        e.preventDefault();

       
        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/searchByName`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name: searchQuery,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                setFilteredProducts(data);
            });
    };

    const handlePriceRangeChange = (event) => {
        const { name, value } = event.target;
        if (name === 'minPrice') {
            setMinPrice(value);
        } else if (name === 'maxPrice') {
            setMaxPrice(value);
        }
    };

    const handlePriceRangeSubmit = (e) => {
        e.preventDefault();

       
        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/searchByPrice`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                minPrice,
                maxPrice,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                setFilteredProducts(data.products);
            });
    };

    return (
        <div id="content">
            <Layout>
                <Container fluid className="mt-3">
                    <Row>
                        <Col md={2}>
                            <Form className="search-bar" onSubmit={handleSearchSubmit}>
                                <Form.Group>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter keyword here..."
                                        value={searchQuery}
                                        onChange={handleSearchChange}
                                    />
                                </Form.Group>
                                <Form.Group className="mt-2 ">
                                    <Button variant="primary" type="submit">
                                        Search
                                    </Button>
                                </Form.Group>
                            </Form>
                            <h3 style={{color:'#5E17EB'}} className="mt-3"><strong>Categories</strong></h3>
                            <ListGroup>
                                <ListGroup.Item
                                    action
                                    href="#"
                                    onClick={() => handleCategorySelect('All')}
                                    active={selectedCategory === 'sports, fps, rpg'}
                                >
                                    All
                                </ListGroup.Item>
                                <ListGroup.Item
                                    action
                                    href="#"
                                    onClick={() => handleCategorySelect('sports')}
                                    active={selectedCategory === 'Sports'}
                                >
                                    Sports
                                </ListGroup.Item>
                                <ListGroup.Item
                                    action
                                    href="#"
                                    onClick={() => handleCategorySelect('rpg')}
                                    active={selectedCategory === 'Role Playing Games'}
                                >
                                    Role Playing Games
                                </ListGroup.Item>
                                <ListGroup.Item
                                    action
                                    href="#"
                                    onClick={() => handleCategorySelect('fps')}
                                    active={selectedCategory === 'Shooting Games'}
                                >
                                    Shooting Games
                                </ListGroup.Item>
                            </ListGroup>

                            <Form onSubmit={handlePriceRangeSubmit}>
                                <Form.Group className="mt-3">
                                    <Form.Label style={{fontSize:'25px', color:'#5E17EB'}}><strong>Price Range:</strong></Form.Label>
                                    <Form.Text>Min</Form.Text>
                                    <Form.Control
                                        type="number"
                                        name="minPrice"
                                        value={minPrice}
                                        onChange={handlePriceRangeChange}
                                        min={0}
                                        max={100}
                                        placeholder="Min Price"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Text>Max</Form.Text>
                                    <Form.Control
                                        type="number"
                                        name="maxPrice"
                                        value={maxPrice}
                                        onChange={handlePriceRangeChange}
                                        min={0}
                                        max={100}
                                        placeholder="Max Price"
                                    />
                                </Form.Group>
                                <Form.Group className="mt-2">
                                    <Button variant="primary" type="submit">
                                        Apply Price Range
                                    </Button>
                                </Form.Group>
                            </Form>
                        </Col>
                        <Col md={10}>
                            <Products searchResults={filteredProducts} />
                        </Col>
                    </Row>
                </Container>
            </Layout>
        </div>
    );
}
