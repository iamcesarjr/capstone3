import React from 'react';
import { Card, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Product(props) {
  const { data } = props;
  const { _id, name, description, price, images } = data;

  return (
    <Col key={_id}>
      <Card className="my-3" bg="dark" text="light" border="info">
        {images.length > 0 && (
          <div className="card-image">
            <Card.Img src={images[0]} alt={name} />
          </div>
        )}
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text className="card-description">{description}</Card.Text>
          <Card.Text>Price: ${price}</Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to={`/products/${_id}`}>
            <Button>Product Details</Button>
          </Link>
        </Card.Footer>
      </Card>
    </Col>
  );
}
