import React from "react";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import Layout from "../components/Layout";
import contactus from '../images/contactus.jpg'
import {BiSolidPhoneCall} from 'react-icons/bi'
import {HiOutlineMailOpen} from 'react-icons/hi'

export default function Contact () {

    return (
        <Layout>
            <Container className="mt-5">
                <Row>
                    <Col md={6} xs={12}>
                        <h1 style={{fontSize:'60px', color:'#5E17EB'}}><strong>Contact Us</strong></h1>
                        <h6 className="mt-2">If you have any concerns, please don't hesitate to call us or send us an email.</h6>

                        <h4><BiSolidPhoneCall /> +63 9023 034 5678</h4>
                        <h4><HiOutlineMailOpen /> gamezoneph@mail.com</h4>

                        <Form className="mt-5">
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" placeholder="name@example.com" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Message</Form.Label>
                                <Form.Control as="textarea" rows={3} />
                            </Form.Group>
                        </Form>
                        <Button variant="primary">Submit</Button>
                    </Col>
                    <Col md={6} xs={12}>
                        <img className="contact-us" src={contactus} alt="contactus"/>
                    </Col>
                </Row>
            </Container>
        </Layout>
    );
};