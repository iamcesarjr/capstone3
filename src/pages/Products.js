import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Card, Col, Row, Container, Button } from 'react-bootstrap';
import Layout from '../components/Layout';
import { Link } from 'react-router-dom';
import ProductDetails from './ProductDetails';

export default function Products({ searchResults }) {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);

  const fetchData = () => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/all`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const displayProducts = searchResults.length > 0 ? searchResults : products;

  const filteredProducts = user.isAdmin
    ? displayProducts
    : displayProducts.filter((product) => product.isActive);

  return (
    <Container fluid>
      <Row xs={1} md={2} lg={3} className="g-4 my-4 py-3">
        {filteredProducts.map((product) => (
          <Col key={product._id}>
            <Card className="product-card" text="light" border="dark">
              {product.images.length > 0 && (
                <div className="card-image">
                  <Card.Img src={product.images[0]} alt={product.name} />
                </div>
              )}
              <Card.Body>
                <Card.Title style={{fontSize:"30px"}}>{product.name}</Card.Title>
                <Card.Text className="card-description">
                  {product.description}
                </Card.Text>
                <Card.Text>Price: ${product.price}</Card.Text>
              </Card.Body>
              <Card.Footer>
                <Link to={`/products/${product._id}`}>
                  <Button variant='info'>Product Details</Button>
                </Link>
              </Card.Footer>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}
