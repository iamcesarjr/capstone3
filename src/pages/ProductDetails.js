import React, { useContext, useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Layout from '../components/Layout';
import AddToCart from './AddToCart';

export default function ProductDetails() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [images, setImages] = useState([]);
  const [quantity, setQuantity] = useState(1);

  useEffect(() => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setImages(data.images);
      })
  }, [productId]);

  return (
    <div id='content'>
      <Layout>
        <Container className="my-5">
          <Row>
            <Col lg={6} md={12} sm={12} className="text-center">
              <Card.Img variant="top" src={images} style={{ width: '500px', height: '500px' }} />
            </Col>
            <Col lg={6} md={12} sm={12} className='text-light'>
              <Card.Body className='card-product'>
                <Card.Title style={{ fontSize: '50px', fontWeight: 'bold' }}>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>$ {price}</Card.Text>
                <Card.Subtitle>Quantity</Card.Subtitle>
                <input
                  type="number"
                  value={quantity}
                  onChange={(e) => setQuantity(e.target.value)}
                  min="1"
                />
                {user.isAdmin ? (
                  <div style={{ marginTop: '20px' }}>
                    <p style={{fontStyle: 'italic', fontSize: '10px'}}>(Admin Account)</p>
                  </div>
                ) : user.id !== null ? (
                  <div style={{ marginTop: '20px' }}>
                    <AddToCart productId={productId} name={name} price={price} quantity={quantity} />
                  </div>
                ) : (
                  <Link className="btn btn-danger" to="/login">
                    Log in to Order
                  </Link>
                )}
                <div style={{ marginTop: '20px' }}>
                  <Link to="/products" style={{ color: '#FFFF', fontWeight: 'bold' }}>
                    Back
                  </Link>
                </div>
              </Card.Body>
            </Col>
          </Row>
        </Container>
      </Layout>
    </div>
  );
}
