
import React, { useEffect, useState } from "react";
import { CardGroup, Col, Container, Row } from "react-bootstrap";
import PreviewProducts from './PreviewProducts'


export default function FeaturedProducts() {
  const [previews, setPreviews] = useState([]);

  useEffect(() => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/all`)
      .then((res) => res.json())
      .then((data) => {
        const numbers = [];
        const featured = [];

        const generateRandomNums = () => {
          let randomNum = Math.floor(Math.random() * data.length);

          if (numbers.indexOf(randomNum) === -1) {
            numbers.push(randomNum);
          } else {
            generateRandomNums();
          }
        }

        for (let i = 0; i < 5; i++) {
          generateRandomNums();

          featured.push(
            <PreviewProducts data={data[numbers[i]]} key={data[numbers[i]]._id} />
          );
        }
        setPreviews(featured);
      });
  }, []);

  return (
    <>
      <Container fluid>
        <Row>
            <Col className="text-center mt-3">
                <h1 style={{backgroundColor: '#5E17EB', color: '#FFFF',fontSize: '80px', fontWeight: 'bold' }}>Featured Products</h1>
            </Col>
        </Row>
      </Container>
      <CardGroup className="justify-content-center">
        {previews}
      </CardGroup>
    </>
  );
}
