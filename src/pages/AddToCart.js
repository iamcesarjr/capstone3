import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function AddToCart ({productId, name, quantity, price}) {

    const [ addedToCart, setAddedToCart ] = useState("")

    const handleAddToCart = () => {

        fetch("https://cpstn2-ecommerceapi-delacruz.onrender.com/users/addToCart", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body:JSON.stringify({
                productId,
                name,
                quantity,
                price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
        if(data === true){
            Swal.fire({
                icon:"success",
                text:"Item Added to Cart"
            })
            setAddedToCart(data)
        }else{
            Swal.fire({
                icon:"error",
                text:"Please try again"
            })
        }
        
        })
    }

    return (
        <>
        {!addedToCart ? (
        <Button variant="success" onClick={handleAddToCart}>
          Add to Cart
        </Button>
      ) : (
        <p>Item added to cart!</p>
      )}
        </>
    )
}