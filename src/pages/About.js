import React from "react";
import { Container, Row, Col } from 'react-bootstrap';
import Layout from "../components/Layout";
import aboutus from '../images/aboutus.jpg'

export default function About () {

    return (
        <Layout>
            <Container className="my-4">
                <Row>
                    <Col md={6} xs={12}>
                        <h1 className="bg-dark p-2 text-white text-center">About Us</h1>
                        <p className="text-justify mt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </Col>
                    <Col md={6} xs={12}>
                        <img src={aboutus} alt="aboutus" style={{ width: '100%', height: '100%' }} />
                    </Col>
                </Row>
            </Container>
        </Layout>
    );
};