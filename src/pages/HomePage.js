import React from "react";
import Layout from "../components/Layout";
import ImageCarousel from "../components/Carousel";
import Banner from "../components/Banner";
import FeaturedProducts from "./FeaturedProduct";

export default function HomePage () {

    return (
        <div id="content">
        <Layout>
            <Banner />
            <ImageCarousel />
            <FeaturedProducts />
        </Layout>
        </div>
    );
};