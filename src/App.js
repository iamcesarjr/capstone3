import {Routes, Route} from 'react-router-dom';
import { useState, useEffect } from 'react';
import './App.css';
import HomePage from './pages/HomePage';
import About from './pages/About';
import Contact from './pages/Contact';
import PageNotFound from './pages/PageNotFound';
import Register from './auth/Register';
import Login from './auth/Login';
import AdminDashboard from './admin/AdminDashboard'

import { UserProvider } from './UserContext';
import Logout from './auth/Logout';
import UserList from './admin/UserList';
import ProductCatalog from './admin/ProductCatalog';
import OrderHistory from './admin/OrderHistory';
import UserDashboard from './user/Dashboard';
import UserCart from './user/Cart';
import UserOrders from './user/UserOrders';
import Profile from './user/Profile';
import Products from './pages/Products';
import ProductPage from './pages/ProductPage';
import ProductDetails from './pages/ProductDetails';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }
        else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })
  }, [user])
  return (
    
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/about" element={<About />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/*" element={<PageNotFound />} />
        <Route path="/adminDashboard" element={<AdminDashboard />} />
        <Route path="/userList" element={<UserList />} />
        <Route path="/productCatalog" element={<ProductCatalog />} />
        <Route path="/orderManager" element={<OrderHistory />} />
        <Route path="/dashboard" element={<UserDashboard />} />
        <Route path="/myCart" element={<UserCart />} />
        <Route path="/myOrder" element={<UserOrders />} />
        <Route path="/myAccount" element={<Profile />} />
        <Route path="/products" element={<ProductPage />} />
        <Route path="/products/:productId" element={<ProductDetails />}/>
  


      </Routes>
    </UserProvider>

  );
}

export default App;
