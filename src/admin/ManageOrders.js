import { useContext, useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function ManageOrders({ user, productId, status }) {
  const [statusValue, setStatusValue] = useState(status); 
  const [users, setUsers] = useState([]);

  const fetchData = () => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/all`)
      .then((res) => res.json())
      .then((data) => {
        setUsers(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const updateStatus = (newStatus, endpoint) => {
    Swal.fire({
      title: "Confirm Update",
      text: `Are you sure you want to update the status to ${newStatus}?`,
      icon: "question",
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/${user._id}/${productId}/${endpoint}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            if (data === true) {
              Swal.fire({
                title: "Success",
                icon: "success",
                text: `Product status updated to ${newStatus}`,
              });
              setStatusValue(newStatus); 
            } else {
              Swal.fire({
                title: "Something Went Wrong",
                icon: "error",
                text: "Please Try again",
              });
            }
          })
          .catch((error) => {
            console.error("Error updating status:", error);
            Swal.fire({
              title: "Server Error",
              icon: "error",
              text: "There was an issue updating the status. Please try again later.",
            });
          });
      }
    });
  };
  
  const updateToShipping = () => {
    updateStatus("AWAITING SHIPPING", "updateShipping");
  };

  const updateToDelivered = () => {
    updateStatus("DELIVERED", "updateDelivered");
  };

  const updateToCancelled = () => {
    updateStatus("CANCELLED", "updateCancelled");
  };

  return (
    <>
      <Button className="mx-2 text-light" variant="warning" onClick={updateToShipping}>Await Shipping</Button>
      <Button className="mx-2" variant="success" onClick={updateToDelivered}>Delivered</Button>
      <Button className="mx-2" variant="danger" onClick={updateToCancelled}>Cancel Order</Button>
    </>
  );
}
