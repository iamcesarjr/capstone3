import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState } from 'react'; 

export default function ArchiveProduct({ product, isActive, fetchData}) {

  const [isProductActive, setIsProductActive] = useState(isActive);

  const archiveToggle = (productId) => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Product successfully disabled'
          });
          
          setIsProductActive(false);
          fetchData()
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'error',
            text: 'Please Try again'
          });
          fetchData()
        }
      });
  };

  const activateToggle = (productId) => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/${productId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Product successfully enabled'
          });
          setIsProductActive(true);
          fetchData()
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'error',
            text: 'Please Try again'
          });
          fetchData()
        }
      });
  };

  return (
    <>
      {isProductActive ? (
        <Button variant="primary" size="sm" onClick={() => archiveToggle(product)}>
          Archive
        </Button>
      ) : (
        <Button variant="success" size="sm" onClick={() => activateToggle(product)}>
          Activate
        </Button>
      )}
    </>
  );
}
