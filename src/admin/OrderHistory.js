import React, { useContext, useState, useEffect } from 'react';
import { Container, Row, Col, Card, Button, Table } from 'react-bootstrap';
import Layout from '../components/Layout';
import AdminMenu from '../components/AdminMenu';
import UserContext from '../UserContext';
import ManageOrders from './ManageOrders';

export default function OrderManager() {
    const { user } = useContext(UserContext);
    const [userOrders, setUserOrders] = useState([]);

    useEffect(() => {
        retrieveOrders();
    }, []);

    const retrieveOrders = () => {
        fetch('https://cpstn2-ecommerceapi-delacruz.onrender.com/users/allOrders', {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                setUserOrders(data);
            });
    };

    return (
        <Layout>
            <Container fluid>
                <Row className='mt-5'>
                    <Col md={3} sm={12}>
                        <AdminMenu />
                    </Col>
                    <Col md={9} sm={12}>
                        <h1 style={{color:'#5E17EB'}}><strong>Manage Orders</strong></h1>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th className='text-center'>User ID</th>
                                    <th className='text-center'>Name</th>
                                    <th className='text-center'>Ordered Items</th>
                                </tr>
                            </thead>
                            <tbody>
                                {userOrders.map((order) => (
                                    <tr key={order._id}>
                                        <td>{order._id}</td>
                                        <td>
                                            {order.firstName} {order.lastName}
                                        </td>
                                        <td>
                                            {order.orderPlaced.map((placedOrder) => (
                                                <ul key={`${order._id}_${placedOrder.productId}`}>
                                                    <li>Product Name: {placedOrder.name}</li>
                                                    <li>Quantity: {placedOrder.quantity}</li>
                                                    <li>Total Price: ${placedOrder.totalPrice}</li>
                                                    <li>{placedOrder.orderPlaced}</li>
                                                    <li>{placedOrder.status}</li>
                                                    <li>
                                                    <p><i>Update order status:</i></p>
                                                        <ManageOrders
                                                            user={order}
                                                            productId={placedOrder.productId}
                                                            status={placedOrder.status}
                                                        />
                                                    </li>
                                                </ul>
                                            ))}
                                        </td>

                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        </Layout>

    );
}
