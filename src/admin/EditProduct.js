import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProduct({ product, fetchData }) {
  const [productId, setProductId] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  const [images, setImages] = useState(product.images || []);
  const [showEdit, setShowEdit] = useState(false);

  const openEdit = (productId) => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductId(data._id);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stock);
      });
    setShowEdit(true);
  };

  const closeEdit = () => {
    setShowEdit(false);
    setName('');
    setDescription('');
    setPrice(0);
    setStock(0);
  };

  const editProduct = (e, productId) => {
    e.preventDefault();

    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        stock: stock,
        images: images,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Product Successfully Updated',
          });
          closeEdit();
          fetchData();
        } else {
          Swal.fire({
            title: 'Error!',
            icon: 'error',
            text: 'Please try again',
          });
          closeEdit();
          fetchData();
        }
      });
  };

  const handleImageUpload = (e) => {
    const file = e.target.files[0]; 
    const reader = new FileReader();

    reader.onload = (event) => {
      
      setImages([event.target.result]);
    };

    reader.readAsDataURL(file);
  };

  return (
    <>
      <Button
        variant="primary"
        size="sm"
        onClick={() => openEdit(product)}
      >
        {' '}
        Edit{' '}
      </Button>

      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={(e) => editProduct(e, productId)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Product Details</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Stock</Form.Label>
              <Form.Control
                type="number"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
              />
            </Form.Group>

            {images.map((image, index) => (
              <img
                key={index}
                src={image}
                alt={`Product ${index + 1}`}
                style={{ maxWidth: '100px', maxHeight: '100px', margin: '5px' }}
              />
            ))}

            <Form.Group>
              <Form.Label>Replace Image:</Form.Label>
              <Form.Control
                type="file"
                accept="image/*"
                onChange={(e) => handleImageUpload(e)}
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
