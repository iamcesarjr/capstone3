import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Layout from '../components/Layout';
import AdminMenu from '../components/AdminMenu';

export default function AdminDashboard () {
    
    
    return(
        <Layout>
            <Container fluid>
                <Row className='mt-5'>
                    <Col className='col-md-3'>
                        <AdminMenu />
                    </Col>
                    <Col className='col-md-9'>
                      <h1>Content:</h1>
                    </Col>
                </Row>
            </Container>
        </Layout>
    )
}