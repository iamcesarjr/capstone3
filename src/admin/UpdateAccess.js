import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UpdateAccess({ user, isAdmin, fetchData }) {
  const [adminAccess, setAdminAccess] = useState(isAdmin);

  const toggleAdmin = () => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/${user._id}/updateAdmin`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Access Updated Successfully',
          });
          setAdminAccess(!adminAccess);
          fetchData();
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'error',
            text: 'Please Try again',
          });
        }
      });
  };

  const toggleRemoveAdmin = () => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/${user._id}/removeAdmin`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Access Updated Successfully',
          });
          setAdminAccess(!adminAccess);
          fetchData();
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'error',
            text: 'Please Try again',
          });
        }
      });
  };

  return (
    <Button
      variant={adminAccess ? 'primary' : 'success'}
      size="sm"
      onClick={adminAccess ? toggleRemoveAdmin : toggleAdmin}
    >
      {adminAccess ? 'Remove Admin Access' : 'Add Admin Access'}
    </Button>
  );
}
