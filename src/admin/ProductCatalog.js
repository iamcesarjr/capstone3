import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Table, Button, Modal } from 'react-bootstrap';
import Layout from '../components/Layout';
import AdminMenu from '../components/AdminMenu';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProducts';
import DeleteProduct from './DeleteProduct';
import AddProduct from './AddProduct';

export default function ProductList() {
    const [products, setProducts] = useState([]);

    const fetchData = () => {

        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/all`)
            .then(res => res.json())
            .then(data => {
                console.log(data)

                setProducts(data);

            });
    }
    useEffect(() => {

        fetchData();

    }, []);

    return (
        <Layout>
            <Container fluid>
                <Row className='mt-5'>
                    <Col md={3} sm={12}>
                        <AdminMenu />
                    </Col>
                    <Col md={9} sm={12}>
                    <h1 style={{color:'#5E17EB'}}><strong>List of Products</strong></h1>
                        <AddProduct fetchData={fetchData}/>

                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th className="text-center">ID</th>
                                    <th className="text-center">Product Name</th>
                                    <th className="text-center">Description</th>
                                    <th className="text-center">Price</th>
                                    <th className="text-center">Stock</th>
                                    <th className="text-center">Category</th>
                                    <th className="text-center">Status</th>
                                    <th colSpan={3} className="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {products.map((product) => (
                                    <tr key={product._id}>
                                        <td>{product._id}</td>
                                        <td>{product.name}</td>
                                        <td>{product.description}</td>
                                        <td>{product.price}</td>
                                        <td>{product.stock}</td>
                                        <td>{product.category}</td>
                                        <td className={product.isActive ? "text-success" : "text-danger"}>
                                            {product.isActive ? "Available" : "Unavailable"}
                                        </td>
                                        <td className="text-center">
                                            <EditProduct product={product._id} fetchData={fetchData} />
                                        </td>
                                        <td className="text-center">
                                            <ArchiveProduct
                                                product={product._id}
                                                isActive={product.isActive}
                                                fetchData={fetchData}
                                            />
                                        </td>
                                        <td className="text-center">
                                            <DeleteProduct
                                                product={product._id}
                                                fetchData={fetchData}
                                            />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        </Layout>
    );
}
