import { useState, useContext } from 'react';
import { Form, Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct({ fetchData }) {
  const { user } = useContext(UserContext);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [category, setCategory] = useState('');
  const [stock, setStock] = useState('');
  const [uploadedImages, setUploadedImages] = useState([]);
  const [imageURLs, setImageURLs] = useState([]);
  const [imageURL, setImageURL] = useState(''); // Added imageURL state
  const [showModal, setShowModal] = useState(false);

  const openModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  function createProduct(e) {
    e.preventDefault();
    let token = localStorage.getItem('token');

    fetch('https://cpstn2-ecommerceapi-delacruz.onrender.com/products/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        category: category,
        stock: stock,
        images: [...uploadedImages, ...imageURLs],
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Product Added',
          });
          fetchData();
          closeModal();
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Unsuccessful Product Creation',
            text: data.message,
          });
        }
      });

    setName('');
    setDescription('');
    setPrice('');
    setCategory('');
    setStock('');
    setUploadedImages([]); // Clear uploadedImages after submission
    setImageURLs([]); // Clear imageURLs after submission
  }

  const handleImageUpload = (e) => {
    const files = e.target.files;
    const uploadedImagesArray = [...uploadedImages];

    for (let i = 0; i < files.length; i++) {
      const reader = new FileReader();
      reader.onload = (e) => {
        uploadedImagesArray.push(e.target.result);
      };
      reader.readAsDataURL(files[i]);
    }

    setUploadedImages(uploadedImagesArray);
  };

  return (
    <>
      <Button className="py-3 my-3" size="lg" variant="primary" onClick={openModal}>
        Add New Product
      </Button>

      <Modal show={showModal} onHide={closeModal}>
        <Form onSubmit={createProduct}>
          <Modal.Header closeButton>
            <Modal.Title>Add New Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group>
              <Form.Label>Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Description"
                required
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Price"
                required
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Category:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Category"
                required
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Stock:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Initial Stock"
                required
                value={stock}
                onChange={(e) => setStock(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Images (Upload or Enter URLs):</Form.Label>
              <Form.Control
                type="file"
                accept="image/*"
                multiple
                onChange={(e) => handleImageUpload(e)}
              />
              <Form.Control
                type="text"
                placeholder="Enter Image URL"
                value={imageURL}
                onChange={(e) => setImageURL(e.target.value)}
              />
              <Button
                variant="primary"
                onClick={() => {
                  setImageURLs([...imageURLs, imageURL]);
                  setImageURL(''); // Clear the input field after adding the URL
                }}
              >
                Add Image URL
              </Button>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" className="my-5">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
