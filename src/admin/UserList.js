import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Table, Form } from 'react-bootstrap';
import Layout from '../components/Layout';
import AdminMenu from '../components/AdminMenu';
import UpdateAccess from './UpdateAccess';

export default function UserList({ user }) {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [searchQuery, setSearchQuery] = useState('');
  const [originalUsers, setOriginalUsers] = useState([]);

  const fetchData = () => {
    fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/users/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        if (Array.isArray(data)) {
          setUsers(data);
          setOriginalUsers(data);
        } else if (typeof data === 'object') {
          setUsers([data]);
          setOriginalUsers([data]);
        } else {
          console.error('Data is not in the expected format:', data);
        }
        setLoading(false);
      })
      .catch((err) => {
        setError(err);
        setLoading(false);
      });
  };


  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Layout>
      <Container fluid>
        <Row className='mt-5'>
          <Col md={3} sm={12}>
            <AdminMenu />
          </Col>
          <Col md={9} sm={12}>
            <h1 style={{color:'#5E17EB'}}><strong>List of Registered Users</strong></h1>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th className="text-center">ID</th>
                  <th className="text-center">Name</th>
                  <th className="text-center">Email</th>
                  <th className="text-center">Mobile No.</th>
                  <th colSpan={2} className="text-center">Access</th>
                </tr>
              </thead>
              <tbody>
                {users.map((user) => (
                  <tr key={user._id}>
                    <td>{user._id}</td>
                    <td>{user.firstName} {user.lastName}</td>
                    <td>{user.email}</td>
                    <td>{user.mobileNo}</td>
                    <td className={user.isAdmin ? "text-success" : "text-danger"}> {user.isAdmin ? "Admin" : "User"}</td>
                    <td className="text-center"> <UpdateAccess user={user} isAdmin={user.isAdmin} fetchData={fetchData} /> </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}
