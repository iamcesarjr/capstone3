import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteProduct({ product, fetchData }) {
  const deleteToggle = (productId) => {

    Swal.fire({
      title: 'Are you sure?',
      text: 'This product will be removed from the Product List',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`https://cpstn2-ecommerceapi-delacruz.onrender.com/products/${productId}/delete`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            if (data === true) {
              Swal.fire({
                title: 'Success',
                icon: 'success',
                text: 'Product deleted',
              });
              fetchData();
            } else {
              Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'Please try again',
              });
              fetchData();
            }
          })
          .catch((err) => console.log(err));
      }
    });
  };

  return (
    <Button variant="danger" size="sm" onClick={() => deleteToggle(product)}>
      Delete
    </Button>
  );
}
